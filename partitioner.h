//
// Created by Lina Qiu on 4/19/20.
//

#ifndef H_PART_PARTITIONER_H
#define H_PART_PARTITIONER_H

#include <list>
#include <algorithm>
#include <climits>
#include <cmath>
#include "virtual_partition.h"

using namespace std;

struct Root {
    VirtualPartition* qualified;
    VirtualPartition* special;
    VirtualPartition* unqualified;
};

class Partitioner {
public:
    vector<VirtualPartition> partitions;
    vector<int>* query_ordering;
    vector<int> real_ordering;
    vector<int>* freq_list;
    vector<int>* qMat;
    int num_data;
    Root root;
    int** queryMat;
    int** dataMat;
    int** cMat;
    short int** dMat; // dependency matrix
    float** unweightedCostMat;
    int dimension;
    int num_empty_slots;
    int page_size;
    bool minmaxBoundaryEnabled;
    bool nearestNeighborFillingEnabled; 
    Partitioner(vector<int>* query_ordering, vector<int>* freq_list, vector<int>* qMat, int num_data, int** queryMat, int** dataMat, short int** dMat, int** cMat, float** unweightedCostMat, int dimension, int num_empty_slots=0, bool minmaxBoundaryEnabled=true, bool nearestNeighborFillingEnabled=true);
    void partitionData(int page_size, int strategy = 1); // 0: naiive method; 1 : correlated query considered
    void naiivePartitionData(list<int>* query_ordering, int page_size);
    vector<int> computeComplement(int q_id, VirtualPartition* parent);
    vector<int> computeIntersection(int q_id, VirtualPartition* parent);
    //VirtualPartition* genSpecialVirtualPartition(vector<int> & qualified_data_idx, vector<int> & unqualified_data_idx);
    void updateRangeFilter(VirtualPartition* vp_p); 
    vector<VirtualPartition> findLeaves(Root root);
    int dfs (VirtualPartition* root, vector<VirtualPartition>* leaves);
    int* getCentralPoint(vector<int> data_idx);
    vector<int> sortByDistance(vector<int>* complement, int* data);

};


#endif //H_PART_PARTITIONER_H

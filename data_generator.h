//
// Created by Lina Qiu on 4/14/20.
//

#ifndef H_PART_DATA_GENERATOR_H
#define H_PART_DATA_GENERATOR_H
#include <random>
#include <iostream>
using namespace std;

class DataGenerator {
public:
    int num_data;
    int dimension;
    int* d_lower, * d_higher;
    int* dis;
    int* p0, * p1;

    DataGenerator(int num_data, int dimension, int* d_lower, int* d_higher, int* data_distribution);
    int** generateData();
    void visualize(int* p, int dim);
};


#endif //H_PART_DATA_GENERATOR_H

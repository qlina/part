//
// Created by Lina Qiu on 4/15/20.
//

#include <iostream>
#include <cmath>
#include <cassert>
#include <algorithm>
#include "query_executor.h"

QueryExecutor::QueryExecutor(int** dataMat, int ndistinct_queryies, int num_data, int dimension) {
    this->dataMat = dataMat;
    this->ndistinct_queries = ndistinct_queryies;
    this->num_data = num_data;
    this->dimension = dimension;
    this->correctnessTestingEnabled = true;
}

int** QueryExecutor::generateCharacteristicMat(int** queryMat) {
    this->queryMat = queryMat;
    cMat = new int*[ndistinct_queries];
    for (int i = 0; i < ndistinct_queries; i++) {
        cMat[i] = new int[num_data];
        for (int j = 0; j < num_data; j++) {
            bool qualified = true;
            // query dimensions
            for (int k = 0; k < dimension; k++) {
                int data = dataMat[j][k];
//                int q_min = queryMat[i][3*k+2];
//                int q_max = queryMat[i][3*k+3];
                int q_min = queryMat[i][2*k];
                int q_max = queryMat[i][2*k+1];
                if (data < q_min || data > q_max) {
                    qualified = false;
                    break;
                }
            }
            if (qualified) {
                cMat[i][j] = 1;
            }
            else {
                cMat[i][j] = 0;
            }
        }
    }
    return cMat;
    // may be able to free queryMat
}

short int** QueryExecutor::generateDependencyMat(int** queryMat){
    vector<float> sel = countSelectivity(); 
    cost = new float*[ndistinct_queries]; 
    dMat = new short int*[ndistinct_queries];
    for(int i = 0; i < ndistinct_queries; i++){
        cost[i] = new float[ndistinct_queries];
        dMat[i] = new short int[ndistinct_queries];
        int* q_i = queryMat[i];
        for(int j = 0; j < ndistinct_queries; j++){
            if(j == i){
                cost[i][j] = 0;
                dMat[i][j] = 1;
            }else{
                cost[i][j] = 1 - sel[j];
                dMat[i][j] = 0;
                int* q_j = queryMat[j];
                bool isOverlapped = true;
                for(int k = 0; k < dimension; k++){
                    if(q_j[2*k] > q_i[2*k+1] || q_j[2*k+1] < q_i[2*k]){
                        isOverlapped = false; 
                        break;
                    }
                } 
                if(!isOverlapped){
                    dMat[i][j] = -1;
                    cost[i][j] -= sel[i];
                }else{
                    bool isInside = true; // whether query j -> query i
                    for(int k = 0; k < dimension; k++){
                        if(q_i[2*k] > q_j[2*k] || q_i[2*k+1] < q_j[2*k+1]){
                            isInside = false;
                            break;
                        }
                    }
                    if(isInside){
                        dMat[i][j] = 1;
                        cost[i][j] = sel[i] - sel[j];
                    }
                    // overlapped but not inside
                }
            } // if-else for j ==i ends
        } // inner loop ends
    }  // outer loop ends
    return dMat;
}

vector<float> QueryExecutor::countSelectivity() {
    vector<float> vec_sel;
    theoretical_total_cost = 0;
    qMat = new vector<int>[ndistinct_queries];
    for (int i = 0; i < ndistinct_queries; i++) {
//        vector<int> q_vec;
        int count = 0;
        for (int j = 0; j < num_data; j++) {
            if (cMat[i][j] == 1) {
                count++;
                qMat[i].push_back(j);
            }
        }
//        qMat[i].push_back(q_vec);
        float sel = (float)count / num_data;
        theoretical_total_cost += count*queryMat[i][2*dimension];
        vec_sel.push_back(sel);
    }
    return vec_sel;
}

void QueryExecutor::prepareTesting(int** queryMat){
    if(qMat == NULL){
        if(cMat == NULL){
            generateCharacteristicMat(queryMat);
        }
        countSelectivity();
    }
}

vector<int>* QueryExecutor::executeQueries(int ndistinct_queries, int** queryMat, vector<VirtualPartition>* partitions, int page_size) {
    if(correctnessTestingEnabled){
        prepareTesting(queryMat); 
    }
    vector<int>* bit_vectors = new vector<int>[ndistinct_queries];


    for (int i = 0; i < ndistinct_queries; i++) {
        int* query = queryMat[i];
        set<int> total_qualified_idx = set<int> ();
        for (int j = 0; j < partitions->size(); j++) {
            //int qualified = 1;
            if(partitions->at(j).queryIsQualified(query)){
                vector<int> qualified_idx = partitions->at(j).data_idx;
                bit_vectors[i].push_back(ceil(qualified_idx.size()*1.0/page_size));
                if(correctnessTestingEnabled){
                //if(true){
                    for(int idx: qualified_idx){
                        total_qualified_idx.insert(idx);
                    }
                }
            }else{
                bit_vectors[i].push_back(0);
            }
        }
        if(correctnessTestingEnabled){
        //if(true){
            for(int idx:qMat[i]){
                if(total_qualified_idx.find(idx) == total_qualified_idx.end()){
                    cout << "Partition Test Failed !!!" << endl;
                    cout << "query id: " << i << "\t unfound data id " << idx << endl;
                    break;
                }
            }
        }
        
    }
    return bit_vectors;
}
// test
vector<int>* QueryExecutor::executeQueries(int num_test_query, int** queryMat, int num_partitions, vector<Cluster>* clusters) {
    if(correctnessTestingEnabled){
        prepareTesting(queryMat); 
    }

    
    vector<int>* qualified_vectors = new vector<int>[num_test_query];
    int q_lower, q_higher, p_lower, p_higher;
    for (int i = 0; i < num_test_query; i++) {
        int* query = queryMat[i];
        set<int> total_qualified_idx = set<int> ();
        for (int j = 0; j < num_partitions; j++) {
            //int** boundary = (*clusters)[j].boundary;
            int qualified = clusters->at(j).queryIsQualified(query);
            /*
            int** boundary = clusters->at(j).boundary; 
            for (int k = 0; k < dimension; k++) {
                q_lower = query[2*k];
                q_higher = query[2*k+1];
                p_lower = boundary[k][0];
                p_higher = boundary[k][1];
                if (p_lower > q_higher ||  p_higher < q_lower) {
                    qualified = 0;
                    break;
                }
            }*/
            
            if(correctnessTestingEnabled){
            //if(true){
                if(qualified){
                    vector<Point> qualified_points = *(clusters->at(j).points);
                    for(Point point : qualified_points){
                        total_qualified_idx.insert(point.getID());
                    }
                }
            }
            qualified_vectors[i].push_back(qualified);
        }
        if(correctnessTestingEnabled){
        //if(true){
            for(int idx:qMat[i]){
                if(total_qualified_idx.find(idx) == total_qualified_idx.end()){
                    cout << "Cluster Partition Test Failed !!!" << endl;
                    cout << "query id: " << i << "\t unfound data id " << idx << endl;
                    break;
                }
                
            }
        }
        
    }
    return qualified_vectors;
}

vector<int>* QueryExecutor::executeQueries(int ndistinct_queries, int** queryMat, vector<Block*>* blockVec_p){
    if(correctnessTestingEnabled){
        prepareTesting(queryMat); 
    }
    

    vector<int>* bit_vectors = new vector<int>[ndistinct_queries];
    for(int i = 0; i < ndistinct_queries; i++){
       int* query = queryMat[i];
       vector<Block*> qualifiedBlockPointers;
       for(int j = 0; j < blockVec_p->size(); j++){
           Block* b_p = blockVec_p->at(j);
           if(b_p->isQualified(query) ){
               bit_vectors[i].push_back(b_p->getNumPages());
               if(correctnessTestingEnabled){
                   qualifiedBlockPointers.push_back(b_p);
               }
           }
       } 
       if(correctnessTestingEnabled){
            for(int idx:qMat[i]){
                bool result = false;
                for(Block* b_p:qualifiedBlockPointers){
                    if(b_p->containsRawRecord(dataMat[idx])){
                        result = true;break;
                    }
                }
                if(!result){
                    cout << "Data-Skipping Partition Test Failed !!!" << endl;
                    cout << "query id: " << i << "\t unfound data id " << idx << endl;
                    break;
                }
            }
        }
    }
    return bit_vectors;
}

//
// Created by Lina Qiu on 4/14/20.
//

#ifndef H_PART_QUERY_GENERATOR_H
#define H_PART_QUERY_GENERATOR_H
#include <list>
#include <random>

using namespace std;

class QueryGenerator {
public:
    int num_queries;
    int dimension;
    int* d_lower;
    int* d_higher;
    vector<int> freq_list;
    vector<int> new_freq_list;
    int ndistinct_queries;
    int** mat;
    vector<int> query_ordering;
    QueryGenerator(int num_queries, int dimension, int* d_lower, int* d_higher, float freq_upper);
    int* generateSingleRandQuery(double d_selectivity, int freq);
    int** generateQueries(float lower, float higher);
    int** regenerateQueries(int** queryMat, float noise=0.0, float noise_lower=0.0, float noise_higher=1.0);
    vector<int> generateFreq(float freq_upper);
    void sortQueries(vector<float>* sel, vector<int>* freq_list_p=NULL);
};


#endif //H_PART_QUERY_GENERATOR_H

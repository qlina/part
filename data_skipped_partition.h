
#ifndef H_PART_DATA_SKIPPED_PARTITION_H
#define H_PART_DATA_SKIPPED_PARTITION_H

#include <vector>
#include <string>
#include <set>
#include <map>
#include "range_filter.h"
using namespace std;

class Cell {
public:
    long id_;
    int freq_;
    int dimension_;
    vector<int> lbs_; // lower bounds for each dimension
    vector<int> ubs_; // upper bounds for each dimension
    Cell (long id, int freq_, vector<int> lbs, vector<int> ubs);
    bool isRecordQualified(int* record);
    bool isQueryIntersected(int* query);
    bool isQueryContained(int* query);

};

class Block {
    set<int*> records_;
    set<int> records_idx_;
    int page_size_;
    bool useless_;
    void updateFeatureVec(int* record);

    static void getRecordFeatureVec(int* record, vector<bool> & recordFeatureVec);
public:
    vector<bool>* blockFeatureVec_p_;
    RangeFilter rangeFilter_;

    Block(int page_size);
    Block(int page_size, set<int*> records);
    ~Block();
    unsigned int size();
    void setPageSize(int page_size);
    unsigned int getNumPages();
    set<int*> getAllRecords();
    set<int> getAllRecordsIdx();
    void insert(int* record, int record_idx);
    void merge(Block* otherBlock_p);
    bool isQualified(int* query);
    bool containsRawRecord(int* record);
    void printBlockFeatureVec();

    static vector<Cell*> s_featureCells_;
    static void getQueryFeatureVec(int* query, vector<bool> & queryFeatureVec);
    static int calculateMergeReward(Block* b1_p, Block* b2_p);
};


class DS_Partitioner {
    vector<int> numBins_;
    int featureSelectPolicy_; // 1: frequency; 2: frequency*(1-sel)
    int dimension_;
    int** queryMat_;
    int** cellularQueryMat_;
    int** dataMat_;
    int** cMat_;
    int num_data_;
    int ndistinct_queries_;
    int numTopFeatures_;
    int* d_lower_;
    int* d_higher_;
    bool qualificationQueryListIndexEnabled_;

    vector<Block*>* blockVec_p_;
    vector<vector<bool> > bitVectorizedData_; // bit vector 
    vector<Cell*> featureCells_;
    vector<vector< int > > global_fences_; // fences in each dimension
    map<long, int> cellReward_; // cell ID -> the reward if partition is based on this cell
    vector<int>* cellFreq_p_;
public:
    int binary_search(int key, vector<int> fences);
    map<long, int> generateCellReward();
    int** generateCellularQueryMat();
    vector<int>* getCellFreqPointer();
    DS_Partitioner(vector<int> & numBins, int** queryMat, int** dataMat, int** cMat, int num_data, int ndistinct_queries, int* d_lower, int* d_higher, int numTopFeatures, int featureSelectPolicy_=1, bool qualificationQueryListIndexEnabled=false);
    ~DS_Partitioner();
    void partitionData(int page_size);
    int getNumTotalCell();
    vector<Block*>* getBlockVecPointer();
    void updateRangeFilter();


};

#endif // H_PART_DATA_SKIPPED_PARTITION_H

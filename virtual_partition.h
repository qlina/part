//
// Created by Lina Qiu on 4/19/20.
//

#ifndef H_PART_VIRTUAL_PARTITION_H
#define H_PART_VIRTUAL_PARTITION_H

#include <vector>
#include <string>
#include "range_filter.h"
using namespace std;


class VirtualPartition {
    //bool keyIsInFilledData(int* key);
    //bool queryContainsFilledData(int* query);
    bool queryIsIntersectedWithBoundary(int* query);
public:
    int dimension;
    RangeFilter rangeFilter;
    string code;
    //vector<int*> filled_data;

    vector<int> data_idx;
    int** minmaxTable;
    VirtualPartition* qualified = NULL;
    VirtualPartition* unqualified = NULL;
    VirtualPartition* special = NULL;
    VirtualPartition* right = NULL;

//    VirtualPartition(vector<int> data_idx);
//    void buildBoundary(int** dataMat, int dimension);
//    void computeBoundary();

    //VirtualPartition(vector<int*> qualifiedRanges, vector<int*> unqualifiedRanges, vector<int> data_idx, vector<int*> filled_data, int dimension);
    VirtualPartition(vector<int*> qualifiedRanges, vector<int*> unqualifiedRanges, vector<int> data_idx, int dimension);
    void buildBoundary(int** dataMat);
    //void fillData(int* data_idx);
    bool keyIsQualified(int* key);
    bool queryIsQualified(int* query);

};


#endif //H_PART_VIRTUAL_PARTITION_H

#include <iostream>
#include <chrono>
#include "parameters.h"
#include "data_generator.h"
#include "query_generator.h"
#include "utility.h"
#include "query_executor.h"
#include "partitioner.h"
#include "evaluator.h"
#include "kmeans.h"
#include "data_skipped_partition.h"


int main() {
    srand(time(0));
    Utility utility = Utility();
    DataGenerator dataGenerator = DataGenerator(num_data, data_dimension, d_lower, d_higher, data_distribution);
    int** dataMat = dataGenerator.generateData();
    QueryGenerator queryGenerator = QueryGenerator(num_queries, data_dimension, d_lower, d_higher, freq_upper);
    int** queryMat = queryGenerator.generateQueries(s_lower, s_higher);
    ndistinct_queries = queryGenerator.freq_list.size();
    QueryExecutor queryExecutor = QueryExecutor(dataMat, ndistinct_queries, num_data, data_dimension);
    /** cMat characteristic matrix distinct_query*data **/
    int** cMat = queryExecutor.generateCharacteristicMat(queryMat);


    /** skipping-oriented **/
    vector<int> num_bins (data_dimension, 6);
    auto start = chrono::high_resolution_clock::now();
    DS_Partitioner ds_partitioner = DS_Partitioner(num_bins, queryMat, dataMat, cMat, num_data, ndistinct_queries, d_lower, d_higher, 32);
    ds_partitioner.partitionData(page_size);
    auto stop = chrono::high_resolution_clock::now();
    auto duration = chrono::duration_cast<chrono::milliseconds>(stop - start);
    cout << "Skipping used time " << duration.count() << " milliseconds" << endl;

    
    /** dependency mat, 0: independent, -1: ((!i) -> j), 1: (j -> i)*/
    short int** dMat = queryExecutor.generateDependencyMat(queryMat);
    /** qMat (qualification mat, distinct_query*listofid) & sel **/
    sel = queryExecutor.countSelectivity();
    /** basic query ordering by f(1-s) **/
    queryGenerator.sortQueries(&sel);

    /** workload-driven **/
    start = chrono::high_resolution_clock::now();
    Partitioner partitioner = Partitioner(&queryGenerator.query_ordering, &queryGenerator.freq_list, queryExecutor.qMat, num_data, queryMat, dataMat, dMat, cMat, queryExecutor.cost, data_dimension);
    partitioner.partitionData(page_size);
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::milliseconds>(stop - start);
    cout << "Our solution used time " << duration.count() << " milliseconds" << endl;

    // out partition based on cellular
    /*int** cellQMat = ds_partitioner.generateCellularQueryMat();
    QueryExecutor cellularQueryExecutor = QueryExecutor(dataMat, ds_partitioner.getNumTotalCell(), num_data, data_dimension);
    int** cellularCMat = cellularQueryExecutor.generateCharacteristicMat(cellQMat);
    short int** cellularDMat = cellularQueryExecutor.generateDependencyMat(cellQMat);
    sel = cellularQueryExecutor.countSelectivity();
    queryGenerator.sortQueries(&sel, ds_partitioner.getCellFreqPointer());
    Partitioner cellularPartitioner = Partitioner(&queryGenerator.query_ordering, ds_partitioner.getCellFreqPointer(), cellularQueryExecutor.qMat, num_data, queryMat, dataMat, cellularDMat, cellularQueryExecutor.cost, data_dimension);
    cellularPartitioner.partitionData(page_size);*/


    /** kmeans **/
    vector<Point> points;
    int** cMat_flipped = utility.flipMatrix(cMat, ndistinct_queries, num_data);
    for (int i = 0; i < num_data; i++) {
        Point point = Point(i, cMat_flipped[i], dataMat[i], ndistinct_queries);
        points.push_back(point);
    }
    start = chrono::high_resolution_clock::now();
    KMeans kmeans(K, num_data, ndistinct_queries, 100, page_size);
    vector<Cluster> clusters = kmeans.run(points, data_dimension, th, queryMat);
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::milliseconds>(stop - start);
    cout << "K-means used time " << duration.count() << " milliseconds" << endl;


    /** evaluation **/
    int** newQueryMat = queryGenerator.regenerateQueries(queryMat, 0.0);
    int ndistinct_test_queries = queryGenerator.new_freq_list.size();

    queryExecutor = QueryExecutor(dataMat, ndistinct_test_queries, num_data, data_dimension);
    cMat = queryExecutor.generateCharacteristicMat(newQueryMat);
    sel = queryExecutor.countSelectivity();
    vector<int>* bitVecs, * bitVecs_compared;
    int count;
//    cout << "page size: " << page_size << endl;
//    cout << "# data: " << num_data << endl;
    cout << "# distinct queries to train: " << ndistinct_queries << endl;
    cout << "# distinct queries to test: " << ndistinct_test_queries << endl;
    cout << "# theoretical minimum IO for test queries: " << queryExecutor.theoretical_total_cost/page_size << endl;
//    cout << "# queries: " << num_queries << endl;

    Evaluator evaluator = Evaluator(newQueryMat, data_dimension, &queryGenerator.new_freq_list);
    start = chrono::high_resolution_clock::now();
    bitVecs = queryExecutor.executeQueries(ndistinct_test_queries, newQueryMat, &partitioner.partitions, page_size);
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::seconds>(stop - start);
    count = evaluator.countQualifiedPartitions(ndistinct_test_queries, bitVecs);
    cout << "Our solution retrieved # pages: " << count << endl;



    /*start = chrono::high_resolution_clock::now();
    bitVecs = cellularQueryExecutor.executeQueries(ndistinct_test_queries, newQueryMat, &cellularPartitioner.partitions, page_size);
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::seconds>(stop - start);
    count = evaluator.countQualifiedPartitions(ndistinct_test_queries, bitVecs);
    cout << "Our cellular solution retrieve # pages: " << count << endl;
    count = evaluator.countNonConsecutiveRuns(ndistinct_test_queries, bitVecs);
    cout << "Our cellular solution retrieve # non-consecutive runs: " << count << endl;*/



    start = chrono::high_resolution_clock::now();
    bitVecs_compared = queryExecutor.executeQueries(ndistinct_test_queries, newQueryMat, K, &clusters);
    stop = chrono::high_resolution_clock::now();
    duration = chrono::duration_cast<chrono::seconds>(stop - start);
    count = evaluator.countQualifiedPartitions(ndistinct_test_queries, bitVecs_compared);
    cout << "Kmeans size limit retrieved # pages: " << count << endl;
//    cout << "Response time: " << duration.count() << " seconds" << endl;


    /*for (int i = 0; i < partitioner.real_ordering.size(); i++) {
        int q_id = partitioner.real_ordering[i];
        int our_pages=0, kmeans_pages=0;
        for (int j : bitVecs[q_id])
            our_pages += j;
        for (int j : bitVecs_compared[q_id])
            kmeans_pages += j;
        cout << "q_id: " << q_id << " " << our_pages << " " << kmeans_pages << endl;
    }*/

    bitVecs = queryExecutor.executeQueries(ndistinct_test_queries, newQueryMat, ds_partitioner.getBlockVecPointer());
    count = evaluator.countQualifiedPartitions(ndistinct_test_queries, bitVecs);
    cout << "Skipping-oriented retrieved # pages: " << count << endl;
    return 0;
}

//
// Created by Lina Qiu on 4/15/20.
//

#ifndef H_PART_QUERY_EXECUTOR_H
#define H_PART_QUERY_EXECUTOR_H

#include <vector>
#include "data_skipped_partition.h"
#include "virtual_partition.h"
#include "kmeans.h"

using namespace std;

class QueryExecutor {
    void prepareTesting(int** queryMat);
public:
    int** cMat;
    short int** dMat;
    float** cost;
    vector<int>* qMat;
    int** queryMat;
    int** dataMat;
    int ndistinct_queries;
    int theoretical_total_cost;
    int num_data;
    int dimension;
    QueryExecutor(int** dataMat, int ndistinct_queries, int num_data, int dimension);
    int** generateCharacteristicMat(int** queryMat);
    short int** generateDependencyMat(int** queryMat);
    bool correctnessTestingEnabled = false;
    vector<float> countSelectivity();
    vector<int>* executeQueries(int ndistinct_queries, int** queryMat, vector<VirtualPartition>* partitions, int page_size);
    vector<int>* executeQueries(int ndistinct_queries, int** queryMat, int num_partitions, vector<Cluster>* clusters);
    vector<int>* executeQueries(int ndistinct_queries, int** queryMat, vector<Block*>* blockVec_p);

};


#endif //H_PART_QUERY_EXECUTOR_H

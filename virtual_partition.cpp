//
// Created by Lina Qiu on 4/19/20.
//

#include <iostream>
#include "virtual_partition.h"

/*
bool VirtualPartition::queryContainsFilledData(int* query){
    for(int* record: filled_data){
        if(RangeFilter::keyIsQualifiedForRange(record, query, dimension)){
            return true;
        }
    }
    return false;
}*/

bool VirtualPartition::queryIsIntersectedWithBoundary(int* query){
    if(minmaxTable != NULL){
       for(int k = 0; k < dimension; k++){
           if(query[2*k] > minmaxTable[k][1] || query[2*k+1] < minmaxTable[k][0]){
               return false;
           }
       } 
    }
    return true;
}

/*
bool VirtualPartition::keyIsInFilledData(int* key){
   bool result =false;
   for(int* record: filled_data){
       result = true; 
       for(int k = 0; k < dimension; k++){
           if(key[k] != record[k]){
               result = false;
               break;
           }
       }
       if(result){
           return true;
       }
   } 
   return false;
}*/

//VirtualPartition::VirtualPartition(vector<int*> qualifiedRanges, vector<int*> unqualifiedRanges, vector<int> data_idx, vector<int*> filled_data, int dimension) {
VirtualPartition::VirtualPartition(vector<int*> qualifiedRanges, vector<int*> unqualifiedRanges, vector<int> data_idx, int dimension) {
    //this->filled_data = filled_data;
    this->rangeFilter = RangeFilter(qualifiedRanges, unqualifiedRanges, dimension);
    this->data_idx = data_idx;
    this->dimension = dimension;
    this->minmaxTable = NULL;
}

void VirtualPartition::buildBoundary(int **dataMat) {
    minmaxTable = new int* [dimension];
    for (int i = 0; i < dimension; i++) {
        minmaxTable[i] = new int[2];
        // initialize the ith dimension min/max value
        minmaxTable[i][0] = dataMat[data_idx[0]][i];
        minmaxTable[i][1] = dataMat[data_idx[0]][i];
    }
    for (int i = 1; i < data_idx.size(); i++) {
        for (int j = 0; j < dimension; j++) {
            if (dataMat[data_idx[i]][j] < minmaxTable[j][0]) {
                minmaxTable[j][0] = dataMat[data_idx[i]][j];
                if (minmaxTable[j][0] == -72)
                    cout << "check" << endl;
            }
            if (dataMat[data_idx[i]][j] > minmaxTable[j][1])
                minmaxTable[j][1] = dataMat[data_idx[i]][j];
        }
    }
//    cout << minmaxTable[0][0] << " " << minmaxTable[0][1] << " " << minmaxTable[1][0] << " " << minmaxTable[1][1] << endl;
}
/*
void VirtualPartition::fillData(int* record){
    filled_data.push_back(record);
}*/

bool VirtualPartition::keyIsQualified(int* key){
    //return keyIsInFilledData(key) || rangeFilter.keyIsQualified(key);   
    return rangeFilter.keyIsQualified(key);   
}


bool VirtualPartition::queryIsQualified(int* query){
    return queryIsIntersectedWithBoundary(query) && rangeFilter.queryIsQualified(query) ;
    //return queryIsIntersectedWithBoundary(query) && ( queryContainsFilledData(query) || rangeFilter.queryIsQualified(query) ) ;
}

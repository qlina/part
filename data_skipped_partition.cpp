#include "data_skipped_partition.h"

#include <set>
#include <map>
#include <string>
#include <queue>
#include <algorithm>
#include <iostream>
#include <climits>

vector<Cell*> Block::s_featureCells_;


inline void mergeBitVecs(vector<bool> * v1_p, vector<bool>* v2_p){
    for(int f = 0; f < v1_p->size(); f++){
        v1_p->at(f) = v1_p->at(f) || v2_p->at(f);

    }
}

void updateCellCounter(int curr_dim, long cellIDPrefix, int freq, vector<int> start_bins, vector<int> end_bins, vector<int> numBins, map<long, int> & cellCounter){
    if(curr_dim == numBins.size()){
        if(cellCounter.find(cellIDPrefix) == cellCounter.end()){
            cellCounter[cellIDPrefix] = 0;
        }
        cellCounter[cellIDPrefix] += freq;
        return;
    }
    cellIDPrefix *= numBins[curr_dim];
    for(int idx = start_bins[curr_dim]; idx <= end_bins[curr_dim]; idx++){
        cellIDPrefix += idx;
        updateCellCounter(curr_dim+1, cellIDPrefix, freq, start_bins, end_bins, numBins, cellCounter); 
    }
}


Cell::Cell(long id, int freq, vector<int> lbs, vector<int> ubs): id_(id), freq_(freq), lbs_(lbs), ubs_(ubs){
    dimension_ = lbs_.size();
}

bool Cell::isRecordQualified(int* record){
    for(int k = 0; k < dimension_; k++){
        if(record[k] < lbs_[k] || record[k] > ubs_[k]){
            return false;
        }
    }
    return true;
}

bool Cell::isQueryIntersected(int *query){
   for(int k = 0; k < dimension_; k++){
       if(query[2*k+1] < lbs_[k] || query[2*k] >ubs_[k]){
           return false; 
       }
   } 
   return true;
}

bool Cell::isQueryContained(int *query){
   for(int k = 0; k < dimension_; k++){
       if(query[2*k+1] > ubs_[k] || query[2*k] < lbs_[k]){
           return false; 
       }
   } 
   return true;
}

Block::Block(int page_size):page_size_(page_size){
    useless_ = false;
    blockFeatureVec_p_ = new vector<bool> (s_featureCells_.size(), false); 
    records_ = set<int*> ();
    records_idx_ = set<int> ();
}

Block::Block(int page_size, set<int*> records):page_size_(page_size), records_(records){
    useless_ = false;
    blockFeatureVec_p_ = new vector<bool> (s_featureCells_.size(), false); 
    for(auto record:records){
        updateFeatureVec(record);
    }
}

Block::~Block(){
    records_.clear();
    blockFeatureVec_p_->clear();
    delete blockFeatureVec_p_;
}

unsigned int Block::size(){return records_.size();}

void Block::setPageSize(int page_size){page_size_ = page_size;}

unsigned int Block::getNumPages(){
    unsigned int size = records_.size();
    if(size % page_size_ == 0){
        return size/page_size_;
    }else{
        return size/page_size_ + 1;
    }
}

set<int*> Block::getAllRecords(){
    return records_; 
}
set<int> Block::getAllRecordsIdx(){
    return records_idx_; 
}
void Block::updateFeatureVec(int* record){
   vector<bool> tmpRecordFeatureVec;
   getRecordFeatureVec(record, tmpRecordFeatureVec);
   useless_ = false;
   for(int f = 0; f < tmpRecordFeatureVec.size(); f++){
       blockFeatureVec_p_->at(f) = blockFeatureVec_p_->at(f) || tmpRecordFeatureVec[f];
       useless_ = blockFeatureVec_p_->at(f) || useless_;
   } 
   useless_ = !useless_;
}


// note that the query feature vector is one more longer than the block feature vector because the last element in query feature represents whether this query is intersected with non-featured cells
void Block::getQueryFeatureVec(int* query, vector<bool> & queryFeatureVec){
    queryFeatureVec.clear();
    queryFeatureVec.resize(s_featureCells_.size()+1, false);
    bool isIntersectedWithNonFeatureCells = true;
    for(int f = 0; f < s_featureCells_.size(); f++){
        queryFeatureVec[f] = s_featureCells_[f]->isQueryIntersected(query);
        if(isIntersectedWithNonFeatureCells){
            isIntersectedWithNonFeatureCells = isIntersectedWithNonFeatureCells && (!s_featureCells_[f]->isQueryContained(query));
        }
    }
    queryFeatureVec[s_featureCells_.size()] = isIntersectedWithNonFeatureCells;
}

void Block::getRecordFeatureVec(int* record, vector<bool> & recordFeatureVec){
    recordFeatureVec.clear();
    recordFeatureVec.resize(s_featureCells_.size(), false);
    for(int f = 0; f < s_featureCells_.size(); f++){
        recordFeatureVec[f] = s_featureCells_[f]->isRecordQualified(record);
    }
}

void Block::insert(int* record, int record_idx) {
    records_.insert(record);
    records_idx_.insert(record_idx);
    updateFeatureVec(record);
}

void Block::merge(Block* otherBlock_p){
    set<int*> otherRecords = otherBlock_p->getAllRecords(); 
    for(auto record:otherRecords){
        records_.insert(record);
    }
    set<int> otherRecords_idx = otherBlock_p->getAllRecordsIdx(); 
    for(auto record_idx:otherRecords_idx){
        records_idx_.insert(record_idx);
    }
    mergeBitVecs(blockFeatureVec_p_, otherBlock_p->blockFeatureVec_p_);
}

int Block::calculateMergeReward(Block * b1_p, Block * b2_p){
    int reward = 0;
    int size1 = b1_p->size();
    int size2 = b2_p->size();
    vector<bool>* blockFeatureVec1_p = b1_p->blockFeatureVec_p_;
    vector<bool>* blockFeatureVec2_p = b2_p->blockFeatureVec_p_;

    for(int f = 0; f < s_featureCells_.size(); f++){
        int freq = s_featureCells_[f]->freq_;        
        if(blockFeatureVec1_p->at(f) == false && blockFeatureVec2_p->at(f) == true){
            reward -= freq*size1;
        }
        
        if(blockFeatureVec1_p->at(f) == true && blockFeatureVec2_p->at(f) == false){
            reward -= freq*size2;
        }
    }  
    return reward;
}

bool Block::isQualified(int* query) {
   if(!this->rangeFilter_.queryIsQualified(query)){
       return false;
   }
   vector<bool> queryFeatureVec;
   Block::getQueryFeatureVec(query, queryFeatureVec);
   
   for(int f = 0; f < blockFeatureVec_p_->size(); f++){
       if(queryFeatureVec[f] && blockFeatureVec_p_->at(f)){
           return true;
       }
   }  
   //return useless_ && queryFeatureVec.back();
   return useless_;
}



bool Block::containsRawRecord(int* record){
    return records_.find(record) != records_.end();
}

void DS_Partitioner::updateRangeFilter(){
    for(int j = 0; j < blockVec_p_->size(); j++){
        Block* block_p = blockVec_p_->at(j);
        block_p->rangeFilter_ = RangeFilter(vector<int*> (), vector<int*> (), dimension_);
        if(block_p->size() == 0) return;
        vector<bool> qualified_cVec = vector<bool> (ndistinct_queries_, true);
        vector<bool> unqualified_cVec = vector<bool> (ndistinct_queries_, false);
        set<int> records_idx = block_p->getAllRecordsIdx();
        for (int record_idx : records_idx){
            for(int j = 0; j < ndistinct_queries_; j++){
                //bool qualified_flag = (point.getValue(j) != 0) ? true : false; ???
                bool qualified_flag = cMat_[j][record_idx];
                qualified_cVec[j] = qualified_cVec[j] && qualified_flag;
                unqualified_cVec[j] = unqualified_cVec[j] || qualified_flag;
            }
        }
        for(int j = 0; j <ndistinct_queries_; j++){
            int* query_idx = queryMat_[j];
            if(qualified_cVec[j]){
                block_p->rangeFilter_.qualifiedRanges.push_back(query_idx);
                
            }
            if(!unqualified_cVec[j]){
                block_p->rangeFilter_.unqualifiedRanges.push_back(query_idx);
            }
        }
    }
}

void Block::printBlockFeatureVec() {
    for(int f = 0; f < blockFeatureVec_p_->size(); f++){
        cout << blockFeatureVec_p_->at(f) << " ";
    }
    cout << endl;
}


DS_Partitioner::DS_Partitioner(vector<int> & numBins, int** queryMat, int** dataMat,int** cMat, int num_data, int ndistinct_queries, int* d_lower, int* d_higher, int numTopFeatures, int featureSelectPolicy, bool qualificationQueryListIndexEnabled): numBins_(numBins), queryMat_(queryMat), dataMat_(dataMat), cMat_(cMat), num_data_(num_data), ndistinct_queries_(ndistinct_queries), d_lower_(d_lower), d_higher_(d_higher), numTopFeatures_(numTopFeatures), featureSelectPolicy_(featureSelectPolicy), qualificationQueryListIndexEnabled_(qualificationQueryListIndexEnabled){
    dimension_ = numBins.size();
    cellFreq_p_ = new vector<int>();
    generateCellReward();
    // select top-reward cells as features
    priority_queue<pair<int, long>, vector<pair<int, long> >, greater<pair<int, long> > > cellFreqQ;
    for(auto iter = cellReward_.begin(); iter != cellReward_.end(); iter++){
       cellFreqQ.push(make_pair(iter->second, iter->first)); 
       if(cellFreqQ.size() > numTopFeatures_ ){
           cellFreqQ.pop();
       }
    }
    
    while(!cellFreqQ.empty()){
        pair<int, long> idWithFreq = cellFreqQ.top();
        cellFreqQ.pop();
        long cellID = idWithFreq.second;
        int freq = idWithFreq.first;
        vector<int> lbs (dimension_, 0);
        vector<int> ubs (dimension_, 0);

        long tmpID = cellID; 
        for(int k = dimension_ - 1; k >= 0; k--){
             int bin_idx = tmpID%numBins[k]; 
             tmpID = tmpID/numBins[k];
             lbs[k] = global_fences_[k][bin_idx];
             ubs[k] = global_fences_[k][bin_idx+1]-1;
        }
        
        featureCells_.push_back(new Cell(cellID, freq, lbs, ubs));
    }

    // PRINT

    //for(int f = 0; f < featureCells_.size(); f ++){
    //    cout << "ID: " << featureCells_[f]->id_ << "\t Frequency: " << featureCells_[f]->freq_ << endl;
    //    for(int k = 0; k < dimension_; k++){
    //        cout << "[" << featureCells_[f]->lbs_[k] << "," << featureCells_[f]->ubs_[k] << "] ";
    //    }
    //    cout << endl;
    //}
    
    
    Block::s_featureCells_ = featureCells_;
}

map<long, int> DS_Partitioner::generateCellReward(){
    // initiailize fences
    for( int k = 0; k < dimension_; k++){
        vector<int> fences_kdim = vector<int>();
        int srange_kdim = d_higher_[k] - d_lower_[k] + 1; 
        int bin_size = srange_kdim/numBins_[k];
        int remaining = srange_kdim%bin_size;
        
        fences_kdim.push_back(d_lower_[k]);
        int tmp_lb = d_lower_[k];
        int tmp_ub = d_lower_[k] + bin_size - 1;
        while(tmp_ub <= d_higher_[k]){
            if(remaining > 0 && tmp_ub < d_higher_[k]){
                tmp_ub += 1;
                remaining--; 
            }
            tmp_lb = tmp_ub + 1;
            tmp_ub = tmp_lb + bin_size - 1;
            fences_kdim.push_back(tmp_lb);
        }
        fences_kdim.push_back(d_higher_[k]+1);
        global_fences_.push_back(fences_kdim);
    }

    // Aggregate Cells
    for(int i = 0; i < ndistinct_queries_; i++){
        vector<int> start_bins; // starting bens in each dimension
        vector<int> end_bins; // ending bins in each dimension
  
        int* query_p = queryMat_[i]; 
        for(int k = 0; k < dimension_; k++){
            int min = query_p[2*k];
            int max = query_p[2*k+1];
            
            if(min < d_lower_[k]) min = d_lower_[k];
            start_bins.push_back(binary_search(min, global_fences_[k]));
           
            if(max > d_higher_[k]) max = d_higher_[k]; 
            end_bins.push_back(binary_search(max, global_fences_[k]));
        }

        updateCellCounter(0, 0l, query_p[2*dimension_], start_bins, end_bins, numBins_, cellReward_);
    } 

    if(featureSelectPolicy_ == 2){
        map<long, int> cellRecordCounter;
        for(int i = 0; i < num_data_; i++){
            vector<int> hit_bins; // ending bins in each dimension
            bool found = true; 
            int* record_p = dataMat_[i]; 
            for(int k = 0; k < dimension_; k++){
                int key = record_p[k];
                int idx = binary_search(key, global_fences_[k]);
                if(idx != -1){
                    hit_bins.push_back(idx);
                }else{
                    found = false;
                    break;
                }
            }
            if(found){
                long cellID = 0l;
                for(int k = 0; k < dimension_; k++){
                    cellID = cellID*numBins_[k] + hit_bins[k];        
                }
                if(cellRecordCounter.find(cellID) != cellRecordCounter.end()){
                    cellRecordCounter[cellID]  += 1;
                }
            }

        } 

        for(auto iter = cellRecordCounter.begin(); iter != cellRecordCounter.end(); iter++){
            int cellID = iter->first;
            if(cellReward_.find(cellID) != cellReward_.end()){
                cellReward_[cellID] *= num_data_ - iter->second;
            }
        } 
      
    }
    return cellReward_;

}

int** DS_Partitioner::generateCellularQueryMat(){
    int cellularQuerySize = cellReward_.size();
    cellularQueryMat_ = new int*[cellularQuerySize];
    int i = 0;
    int freqIdx = dimension_*2;
    for(auto iter = cellReward_.begin(); iter != cellReward_.end(); iter++){
        cellularQueryMat_[i] = new int[freqIdx+1];
        long tmpID = iter->first; 
        for(int k = dimension_ - 1; k >= 0; k--){
             int bin_idx = tmpID%numBins_[k]; 
             tmpID = tmpID/numBins_[k];
             cellularQueryMat_[i][2*k] = global_fences_[k][bin_idx];
             cellularQueryMat_[i][2*k+1]= global_fences_[k][bin_idx+1]-1;
        } 
        cellularQueryMat_[i][freqIdx] = iter->second;
        cellFreq_p_->push_back(iter->second);;
        i++;
    }
    return cellularQueryMat_ ;

}

int DS_Partitioner::getNumTotalCell(){return cellReward_.size();}

vector<int>* DS_Partitioner::getCellFreqPointer(){
    return cellFreq_p_;
}


DS_Partitioner::~DS_Partitioner(){
    for(int b_idx = 0; b_idx < blockVec_p_->size(); b_idx++){
        if(blockVec_p_->at(b_idx) != NULL){
            delete blockVec_p_->at(b_idx);
        }
    }
    delete blockVec_p_;
}


void DS_Partitioner::partitionData(int page_size){
    // initialize every record as a block
    map<vector<bool>, Block*> blockMap;
    for(int r_idx = 0; r_idx < num_data_; r_idx++){
        Block* b_p = new Block(page_size);
        b_p->insert(dataMat_[r_idx], r_idx);
        vector<bool> blockFeatureVec = *(b_p->blockFeatureVec_p_);
        if(blockMap.find(blockFeatureVec) == blockMap.end()){
            blockMap[blockFeatureVec] = b_p;
        }else{
            blockMap[blockFeatureVec]->merge(b_p);        
            delete b_p;
        }
    }

    //cout << blockMap.size() << endl;
    blockVec_p_ = new vector<Block*>();
    vector<Block*> tmpBlockVec;
    set<Block*> tmpBlockSet;
    for(auto iter = blockMap.begin(); iter != blockMap.end(); iter++){
        Block* b_p = iter->second;
        if(b_p->size() >= page_size){
            blockVec_p_->push_back(b_p);
        }else{
            tmpBlockVec.push_back(b_p);
            tmpBlockSet.insert(b_p);
        }
    } 


    // max skip partitioning
    map<pair<int, int>, int> mergeOptions;
    //map<int, string> mergeHistory;
   
    int numBlocks = tmpBlockVec.size(); 

    //for(int b1_idx = 0; b1_idx < numBlocks; b1_idx++){ mergeHistory[b1_idx] = to_string(b1_idx);}

    for(int b1_idx = 0; b1_idx < numBlocks - 1; b1_idx++){
         for(int b2_idx = b1_idx + 1; b2_idx < numBlocks; b2_idx++){
             mergeOptions[make_pair(b1_idx, b2_idx)] = Block::calculateMergeReward(tmpBlockVec[b1_idx], tmpBlockVec[b2_idx]); 
         }
    }

    while(!tmpBlockSet.empty()){
        if(tmpBlockSet.size() == 1){
            Block* b_p = *tmpBlockSet.begin();
            blockVec_p_->push_back(*tmpBlockSet.begin());
            tmpBlockSet.erase(b_p);

        }else{
           int max = INT_MIN;
           int b1_idx, b2_idx;
           for(auto iter = mergeOptions.begin(); iter != mergeOptions.end(); iter++){
              if(iter->second > max){
                  b1_idx = iter->first.first;
                  b2_idx = iter->first.second;
                  max = iter->second;
              } 
           } 

           Block* b1_p = tmpBlockVec[b1_idx];
           Block* b2_p = tmpBlockVec[b2_idx];
           b1_p->merge(b2_p);
           //cout << b1_idx << "Merging " << mergeHistory[b1_idx] << " with " << mergeHistory[b2_idx] << endl;
           //mergeHistory[b1_idx] += "-" + mergeHistory[b2_idx];
           //mergeHistory.erase(b2_idx);

           // remove all merge options related to b2
           tmpBlockSet.erase(b2_p);
           for(int b_idx = 0; b_idx < numBlocks; b_idx++){
               if(b_idx != b2_idx && tmpBlockVec[b_idx] != NULL){
                  pair<int, int> removingPair;
                  if(b_idx > b2_idx){
                      removingPair = make_pair(b2_idx, b_idx);
                  }else{
                      removingPair = make_pair(b_idx, b2_idx);
                  } 
                  if(mergeOptions.find(removingPair) != mergeOptions.end()){
                      mergeOptions.erase(removingPair);
                  }
               } 
           }
       
           delete b2_p;
               

           b2_p = NULL;
           tmpBlockVec[b2_idx] = NULL;

           // update block set and merge options          
           if(b1_p->size() >= page_size){
               blockVec_p_->push_back(b1_p);
               tmpBlockSet.erase(b1_p);
               // remove all merge options related to b1 because b1 exceeds page size limit
               for(int b_idx = 0; b_idx < numBlocks; b_idx++){
                   if(b_idx != b1_idx){
                      pair<int, int> removingPair;
                      if(b_idx > b1_idx){
                          removingPair = make_pair(b1_idx, b_idx);
                      }else{
                          removingPair = make_pair(b_idx, b1_idx);
                      }
                      if(mergeOptions.find(removingPair) != mergeOptions.end()){
                          mergeOptions.erase(removingPair);
                      }
                   } 
               }   
               tmpBlockVec[b1_idx] = NULL;
           }else{
               // update merge options for the updated b1_p
               for(int b_idx = 0; b_idx < numBlocks; b_idx++){
                   if(b_idx != b1_idx && tmpBlockVec[b_idx] != NULL){
                       if(b_idx > b1_idx){
                           mergeOptions[make_pair(b1_idx, b_idx)] = Block::calculateMergeReward(tmpBlockVec[b_idx], tmpBlockVec[b1_idx]);
                       }else{
                           mergeOptions[make_pair(b_idx, b1_idx)] = Block::calculateMergeReward(tmpBlockVec[b_idx], tmpBlockVec[b1_idx]);
                       }
                   }
               }
           }

        } // if-else for size == 1 ends
    } // while loop ends  

    // PRINT
    //for(int b_idx = 0; b_idx < blockVec_p_->size(); b_idx++){
    //    cout << blockVec_p_->at(b_idx)->blockFeatureVec_p_->size() << " ";
    //}
    //cout << endl;
}



int DS_Partitioner::binary_search(int key, vector<int> fences){
    int len = fences.size();
    if(len <= 1){return -1;}
    int start = 0;
    int end = len - 1;
    int mid;

    if(key < fences[start]) { return -1;}
    if(key >= fences[end]) {return -1;}

    while(end - start > 1){
        mid = (start + end)/2;
        if(key<fences[mid]){
            end = mid;
        }else{
            start = mid;
        }
    }
    return start;
}


vector<Block*>* DS_Partitioner::getBlockVecPointer(){
    return blockVec_p_;
}


//
// Created by Lina Qiu on 4/14/20.
//

#include "query_generator.h"
#include <ctime>
#include <cstdlib>
#include <cmath>
#include <cstring>
#include <chrono>
#include <algorithm>

QueryGenerator::QueryGenerator(int num_queries, int dimension, int* d_lower, int* d_higher, float freq_upper) {
    this->num_queries = num_queries;
    this->dimension = dimension;
    this->d_lower = d_lower;
    this->d_higher = d_higher;
    this->freq_list = generateFreq(freq_upper);
    this->ndistinct_queries = freq_list.size();
}

int* QueryGenerator::generateSingleRandQuery(double d_selectivity, int freq){
    int dim_times = dimension * 2;
    int* q = new int[dim_times+1]; // range query requires dimension*2, one d for freq, (one d for #qualification(selectivity))
    int min, range, max;
    for (int j = 0; j < dimension; j++) {
        // restrict min to the fst 60% of data range
        min =  rand() % int((d_higher[j] - d_lower[j])* 0.6) + d_lower[j];
        range = (d_higher[j] - d_lower[j]) * d_selectivity;
        max = (min + range) > d_higher[j] ? d_higher[j] : (min + range);
        q[2*j] = min;
        q[2*j+1] = max;
    }
    q[dim_times] = freq;
    return q;
}

// lower, higher refer to predicted bounds of selectivity
int** QueryGenerator::generateQueries(float lower, float higher) {
    mat = new int*[ndistinct_queries];
    float srange = higher - lower;
    int dim_times = dimension * 2;
    int min, range, max;
    vector<int>::iterator it = freq_list.begin();
    for (int i = 0; i < ndistinct_queries; i++) {
        double selectivity = rand() / (RAND_MAX / srange) + lower;
        // may have different selectivity for different dimension
        double d_selectivity = pow(selectivity, 1.0/dimension);
        // adjust range according to selectivity
        mat[i] = generateSingleRandQuery(d_selectivity, *it);
        it++;
    }
    return mat;
}

// regenerate queries
int** QueryGenerator::regenerateQueries(int** oldQueryMat, float noise, float noise_lower, float noise_higher){
    int freq_idx = 2*dimension;
    new_freq_list = vector<int> ();
    vector<pair<int*, int> > queryCounter = vector<pair<int*, int> > (ndistinct_queries);
    for(int i = 0; i < ndistinct_queries; i++){
        queryCounter[i] = make_pair(oldQueryMat[i], oldQueryMat[i][freq_idx]);
    }
    int num_noise_queries = round(num_queries*noise);
    if(num_noise_queries + ndistinct_queries > num_queries){ // we have to omit some existing queries when the noise is too large
       random_shuffle(queryCounter.begin(), queryCounter.end()); 
       ndistinct_queries = num_queries - num_noise_queries;
       queryCounter = vector<pair<int*, int> > (queryCounter.begin(), queryCounter.begin() + ndistinct_queries);
    } 

    int** newMat = new int*[ndistinct_queries + num_noise_queries];
    int num_existing_queries = 0;
    default_random_engine generator;
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    int i = 0;
    for(; i < ndistinct_queries - 1; i++){
       newMat[i] = new int[freq_idx+1];
       memcpy(newMat[i], oldQueryMat[i], sizeof(int)*(freq_idx+1));
       //generate the frequency according to the poisson distribution
       poisson_distribution<int> distribution (oldQueryMat[i][freq_idx]*(1-noise));
       int new_freq = distribution(generator);
       newMat[i][freq_idx] = new_freq; 
       num_existing_queries += new_freq;
       new_freq_list.push_back(new_freq);
    }
    // the frequency of the last query is set according to the number of existing queries
    newMat[i] = new int[freq_idx+1];
    memcpy(newMat[i], oldQueryMat[i], sizeof(int)*(freq_idx+1));
    newMat[i][freq_idx] = num_queries - num_existing_queries - num_noise_queries;
    new_freq_list.push_back(newMat[i][freq_idx]);
    i++;

    float srange = noise_higher - noise_lower;
    for(int j = 0; j < num_noise_queries; j++){
        double selectivity = rand() / (RAND_MAX / srange) + noise_lower;
        // may have different selectivity for different dimension
        double d_selectivity = pow(selectivity, 1.0/dimension);
        // adjust range according to selectivity

        newMat[i+j] = generateSingleRandQuery(d_selectivity, 1);
        new_freq_list.push_back(1);
    }
    return newMat;
}

vector<int> QueryGenerator::generateFreq(float freq_upper) {
    vector<int> freqList;
    int upperbound = freq_upper * num_queries;
    default_random_engine generator;
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    uniform_int_distribution<int> distribution(1, upperbound);
    for (int count = num_queries; count > 0;) {
        int number = distribution(generator);
        if (number > count)
            number = count;
        freqList.push_back(number);
        count -= number;
    }
    return freqList;
}

// sort by f(1-s)
void QueryGenerator::sortQueries(vector<float>* sel, vector<int>* freq_list_p) {
    query_ordering.clear();
    vector<pair<float, int>> ordering;
    int dim_times = dimension * 2;
    vector<float>& selRef = *sel;
    if(freq_list_p == NULL){
        for (int i = 0; i < sel->size(); i++){
            float product = mat[i][dim_times] * (1 - selRef[i]);
            ordering.push_back(pair<float, int>(product, i));
        }
    }else{
        for (int i = 0; i < sel->size(); i++){
            float product = freq_list_p->at(i) * (1 - selRef[i]);
            ordering.push_back(pair<float, int>(product, i));
        }
    }
    // sort in descending order
    sort(ordering.begin(), ordering.end(),greater<pair<float, int>>());
    for (int i = 0; i < sel->size(); i++) {
        query_ordering.push_back(ordering[i].second);
    }
}

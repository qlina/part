//
// Created by Lina Qiu on 4/14/20.
//

#include <chrono>
#include "data_generator.h"
DataGenerator::DataGenerator(int num_data, int dimension, int* d_lower, int* d_higher, int* data_distribution) {
    this->num_data = num_data;
    this->dimension = dimension;
    this->d_lower = d_lower;
    this->d_higher = d_higher;
    this->dis = data_distribution;
    // for visualization
    this->p0 = new int[d_higher[0] - d_lower[0]];
    this->p1 = new int[d_higher[1] - d_lower[1]];
}

int** DataGenerator::generateData() {
    int ** data_mat = new int *[num_data];
    /// not truly random
    default_random_engine generator;
    generator.seed(std::chrono::system_clock::now().time_since_epoch().count());
    // initialization
    for (int i = 0; i < num_data; i++) {
        data_mat[i] = new int[dimension];
    }
    for (int j = 0; j < dimension; j++) {
        switch (dis[j]) {
            case 1: {
                int mean = (d_lower[j] + d_higher[j]) / 2;
                int stddev = (d_higher[j] - d_lower[j]) / 4;
                normal_distribution<double> distribution(mean, stddev);
                for (int i = 0; i < num_data; i++) {
                    double number = distribution(generator);
                    while (!((number >= d_lower[j])&&(number <= d_higher[j]))) {
                        number = distribution(generator);
                    }
                    ++p1[int(number)];
                    data_mat[i][j] = int(number);
                }
                break;
            }
            case 0: {
                uniform_int_distribution<int> distribution(d_lower[j],d_higher[j]);
                for (int i = 0; i < num_data; i++) {
                    int number = distribution(generator);
                    while (!((number >= d_lower[j])&&(number <= d_higher[j]))) {
                        number = distribution(generator);
                    }
                    ++p0[number];
                    data_mat[i][j] = number;
                }
                break;
            }
            default:
                cout << "Unexpected case" << endl;
        }
    }
    return data_mat;
}


// (pi, i)
void DataGenerator::visualize(int* p, int dim) {
    for (int i = 0; i < (d_higher[dim]-d_lower[dim]+1); ++i) {
        std::cout << i << "-" << (i+1) << ": ";
        std::cout << std::string(p[i],'*') << std::endl;
    }
}

//
// Created by Lina Qiu on 4/15/20.
//

#include "utility.h"

void Utility::printMat(int **mat, int d1, int d2) {
    for (int i = 0; i < d1; i++) {
        for (int j = 0; j < d2; j++) {
            cout << mat[i][j] << " ";
        }
        cout << endl;
    }
    cout << endl;
}

int** Utility::flipMatrix(int** cMat, int ndistinct_queries, int num_data) {
    int** cMat_flipped = new int*[num_data];
    for (int i = 0; i < num_data; i++) {
        cMat_flipped[i] = new int[ndistinct_queries];
        for (int j = 0; j < ndistinct_queries; j++) {
            cMat_flipped[i][j] = cMat[j][i];
        }
    }
    return cMat_flipped;
}
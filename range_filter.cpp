#include <iostream>
#include "range_filter.h"

RangeFilter::RangeFilter(){
    this->qualifiedRanges = vector<int*> ();
    this->unqualifiedRanges = vector<int*> ();
}

RangeFilter::RangeFilter(vector<int*> qualifiedRanges, vector<int*> unqualifiedRanges, int dimension){
   this->qualifiedRanges = qualifiedRanges;
   this->unqualifiedRanges = unqualifiedRanges;
   this->dimension = dimension; 
}

bool RangeFilter::keyIsQualifiedForRange(int* key, int* range, int dimension){
    for(int k = 0; k < dimension; k++){
        if(key[k] < range[2*k] || key[k] > range[2*k+1]){
            return false;
        }
    }
    return true;
}



bool RangeFilter::queryIsInsideRange(int* query, int* range){
    for(int k = 0; k < dimension; k++){
        if(range[2*k+1] < query[2*k+1] || range[2*k] > query[2*k]){
            return false;
        }
    }
    return true;


}

bool RangeFilter::queryIsQualifiedForRange(int* query, int* range){
    for(int k = 0; k < dimension; k++){
        if(range[2*k+1] < query[2*k] || range[2*k] > query[2*k+1]){
            return false;
        }
    }
    return true;
}

bool RangeFilter::keyMayQualified(int* key, vector<int*> ranges, bool existing_vs_every){
    for(int* r: ranges){
        if(existing_vs_every == keyIsQualifiedForRange(key, r, dimension)){
            return existing_vs_every;
        }
    }
    return !existing_vs_every;
}

bool RangeFilter::queryMayQualified(int* query, vector<int*> ranges, bool existing_vs_every){
    for(int* r: ranges){
        if(existing_vs_every == queryIsQualifiedForRange(query, r)){
            return existing_vs_every;
        }
    }  
    return !existing_vs_every;
}

bool RangeFilter::keyIsQualified(int* key){
    if(qualifiedRanges.size() != 0 && !keyMayQualified(key, qualifiedRanges, false)){
        return false;
    }

    if(unqualifiedRanges.size() != 0 && keyMayQualified(key, unqualifiedRanges, true)){
        return false;
    }
    
    return true;
}

bool RangeFilter::queryIsQualified(int* query){
    if(qualifiedRanges.size() != 0 && !queryMayQualified(query, qualifiedRanges, false))
        return false;

    if(unqualifiedRanges.size() != 0){ 
        for(int* r: unqualifiedRanges){ 
            if(queryIsInsideRange(query, r)){
                return false;
            }
        } 
    }
    return true;
}

void RangeFilter::print(){
    cout << "Qualified Queries List:" << endl;
    cout << "==============================" << endl;
    for(int* query:qualifiedRanges){
        for(int k = 0; k < dimension; k++){
            cout << "(" << query[2*k] << "," << query[2*k+1] << ") ";
        }
        cout << endl;
    }
    cout << "==============================" << endl;
    
    cout << "Unualified Queries List:" << endl;
    cout << "==============================" << endl;
    for(int* query:unqualifiedRanges){
        for(int k = 0; k < dimension; k++){
            cout << "(" << query[2*k] << "," << query[2*k+1] << ") ";
        }
        cout << endl;
    }
    cout << "==============================" << endl;

}



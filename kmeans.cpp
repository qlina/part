//
// Created by Lina Qiu on 4/22/20.
//

#include "kmeans.h"

Point::Point(int id_point, int* values, int* raw_values, int dimension) {
    this->id_point = id_point;
    this->values = values;
//        for(int i = 0; i < dimension; i++)
//            this->values.push_back(values[i]);
    this->raw_values = raw_values;
    this->dimension = dimension;
    float sum = 0.0;
    for (int d = 0; d < dimension; d++) {
        sum += pow(values[d], 2.0);
    }
    this->min_dist = sqrt(sum);
//        cout << id_point << " initial dist " << this->min_dist << endl;
    id_cluster = -1;
}

int Point::getID()
{
    return id_point;
}

void Point::setCluster(int id_cluster)
{
    this->id_cluster = id_cluster;
}

int Point::getCluster()
{
    return id_cluster;
}

void Point::setMinDist(float min_dist) {
    this->min_dist = min_dist;
}

float Point::getMinDist() {
    return this->min_dist;
}

float Point::getValue(int index)
{
    return (float)values[index];
}

int Point::getRawValue(int index) {
    return raw_values[index];
}

int Point::getDimension()
{
    return dimension;
}



Cluster::Cluster(int id_cluster, Point point, int data_dimension)
{
    this->id_cluster = id_cluster;
    this->size = 1;
    dimension = point.getDimension();
    this->points = new vector<Point>();
    for(int i = 0; i < dimension; i++)
        central_values.push_back(point.getValue(i));

    points->push_back(point);
    this->data_dimension = data_dimension;
    this->boundary = NULL;
}

Cluster::Cluster(int id_cluster, int data_dimension)
{
    this->id_cluster = id_cluster;
    this->data_dimension = data_dimension;
    this->boundary = NULL;
}

void Cluster::addPoint(Point point)
{
    points->push_back(point);
}

bool Cluster::removePoint(int id_point)
{
    int total_points = points->size();

    for(int i = 0; i < total_points; i++)
    {
        if(points->at(i).getID() == id_point)
        {
            points->erase(points->begin() + i);
            decreaseSize();
            return true;
        }
    }
    return false;
}

float Cluster::getCentralValue(int index)
{
    return central_values[index];
}

void Cluster::setCentralValue(int index, float value)
{
    central_values[index] = value;
}

Point Cluster::getPoint(int index)
{
    return points->at(index);
}

void Cluster::resetSize() {
    this->size = 0;
}

int Cluster::getSize()
{
    return size;
}

void Cluster::increaseSize() {
    this->size++;
}

void Cluster::decreaseSize() {
    this->size--;
}

int Cluster::getTotalPoints()
{
    return points->size();
}

int Cluster::getID()
{
    return id_cluster;
}

void Cluster::buildBoundary(int data_dimension) {
    boundary = new int*[data_dimension];
    for (int i = 0; i < data_dimension; i++) {
        boundary[i] = new int[2];
        boundary[i][0] = boundary[i][1] = points->at(0).getRawValue(i);
    }
    for (int j = 1; j < points->size(); j++) {
        for (int i = 0; i < data_dimension; i++) {
            if (points->at(j).getRawValue(i) < boundary[i][0])
                boundary[i][0] = points->at(j).getRawValue(i);
            else if (points->at(j).getRawValue(i) > boundary[i][1])
                boundary[i][1] = points->at(j).getRawValue(i);
        }
    }
}

void Cluster::computeMajority(int num_queries) {
    dimension = num_queries;
    majority = new int[dimension];
//        cout << points[0].getValue(0) << points[0].getRawValue(1) << points[0].getRawValue(2) << endl;
//        cout << points[1].getValue(0) << points[1].getRawValue(1) << points[1].getRawValue(2) << endl;
//        cout << points[2].getValue(0) << points[2].getRawValue(1) << points[2].getRawValue(2) << endl;
    for (int i = 0; i < dimension; i++) {
        int num_zeros = 0, num_ones = 0;
        for (Point point : (*points)){
            if (point.getValue(i) == 0)
                num_zeros++;
            else
                num_ones++;
        }
        if (num_zeros >= num_ones)
            majority[i] = 0;
        else
            majority[i] = 1;
    }
}

void Cluster::computeCosSimilarity() {
    float inner = 0;
    float dis = 0;
    for (int i = 0; i < dimension; i++) {
        inner += majority[i];
    }
    dis = sqrt(inner);
    dis *= sqrt(dimension);
    if (dis == 0)
        cos_similarity = 0;
    else
        cos_similarity = inner / dis;
}

void Cluster::updateRangeFilter(int** queryMat){
    rangeFilter = RangeFilter(vector<int*> (), vector<int*> (), data_dimension);
    if(size == 0) return;
    vector<bool> qualified_cVec = vector<bool> (dimension, true);
    vector<bool> unqualified_cVec = vector<bool> (dimension, false);
    for (Point point : (*points)){
        for(int j = 0; j < dimension; j++){
            bool qualified_flag = (point.getValue(j) != 0) ? true : false;
            qualified_cVec[j] = qualified_cVec[j] && qualified_flag;
            unqualified_cVec[j] = unqualified_cVec[j] || qualified_flag;
        }
    }
    for(int j = 0; j <dimension; j++){
        int* query_idx = queryMat[j];
        if(qualified_cVec[j]){
            rangeFilter.qualifiedRanges.push_back(query_idx);
            
        }
        if(!unqualified_cVec[j]){
            rangeFilter.unqualifiedRanges.push_back(query_idx);
        }
    }
    //cout << "QL Size: " << rangeFilter.qualifiedRanges.size() << " - " << "UQL Size: " << rangeFilter.unqualifiedRanges.size() << endl;
}



// return ID of nearest center (uses euclidean distance)
//int KMeans::getIDNearestNonFullCenter(Point& point)
//{
//    float sum = 0.0, min_dist;
//    int c = 0;
//    for (; c < K; c++) {
//        if (clusters[c].getTotalPoints() < cluster_size)
//            break;
//    }
//    int id_cluster_center = c;
//    for (int i = 0; i < total_values; i++)
//    {
//        sum += pow(clusters[c].getCentralValue(i) -
//                   point.getValue(i), 2.0);
//    }
//
//    min_dist = sqrt(sum);
//
//    for(int i = c+1; i < K; i++)
//    {
//        float dist;
//        sum = 0.0;
//
//        for(int j = 0; j < total_values; j++)
//        {
//            sum += pow(clusters[i].getCentralValue(j) -
//                       point.getValue(j), 2.0);
//        }
//
//        dist = sqrt(sum);
//        if (clusters[i].getTotalPoints()  >= cluster_size) {
//            cout << "mind" << endl;
//        }
//        if(dist < min_dist && clusters[i].getTotalPoints() < cluster_size)
//        {
//            min_dist = dist;
//            id_cluster_center = i;
//        }
//    }
//    point.setMinDist(min_dist);
//    return id_cluster_center;
//}

int KMeans::getIDNearestNonFullCenter(Point& point)
{
    float sum = 0.0, min_dist;
    vector<pair<float,int>> dist_vec;
//    int id_cluster_center = 0;
    for (int i = 0; i < ndistinct_queries; i++)
    {
        sum += pow(clusters[0].getCentralValue(i) -
                   point.getValue(i), 2.0);
    }
    min_dist = sqrt(sum);
    dist_vec.push_back(pair<float, int>(min_dist, 0));
    for(int i = 1; i < K; i++)
    {
        float dist;
        sum = 0.0;
        for(int j = 0; j < ndistinct_queries; j++)
        {
            sum += pow(clusters[i].getCentralValue(j) -
                       point.getValue(j), 2.0);
        }

        dist = sqrt(sum);
        dist_vec.push_back(pair<float, int>(dist, i));
    }
    sort(dist_vec.begin(), dist_vec.end());
    for (int i = 0; i < K; i++){
        if (clusters[dist_vec[i].second].getSize() < cluster_size) {
            point.setMinDist(dist_vec[i].first);
            return dist_vec[i].second;
        }
    }
    cout << "fault" << endl;
//    point.setMinDist(min_dist);
//    return id_cluster_center;
}

void KMeans::resetClusterSizeLimit() {
    for(int i = 0; i < K; i++) {
        clusters[i].resetSize();
        clusters[i].points->clear();
        clusters[i].points = new vector<Point>();

    }
}

KMeans::KMeans(int K, int total_points, int ndistinct_queries, int max_iterations, int cluster_size, bool minmaxBoundaryEnabled)
{
    this->K = K;
    this->total_points = total_points;
    this->ndistinct_queries = ndistinct_queries;
    this->max_iterations = max_iterations;
    this->cluster_size = cluster_size;
    this->rss = numeric_limits<float>::max();
    this->minmaxBoundaryEnabled = minmaxBoundaryEnabled;
}

vector<Cluster> KMeans::run(vector<Point>& points, int data_dimension, float th, int** queryMat)
{
    if(K > total_points)
        return vector<Cluster>();

    vector<int> prohibited_indexes;

    // choose K distinct values for the centers of the clusters
    for(int i = 0; i < K; i++)
    {
        while(true)
        {
            int index_point = rand() % total_points;

            if(find(prohibited_indexes.begin(), prohibited_indexes.end(),
                    index_point) == prohibited_indexes.end())
            {
                prohibited_indexes.push_back(index_point);
                points[index_point].setCluster(i);
                Cluster cluster(i, points[index_point], data_dimension);
                clusters.push_back(cluster);
                break;
            }
        }
    }

    int iter = 1;

    while(true)
    {
        bool done = true;

        // associates each point to the nearest center
        for(int i = 0; i < total_points; i++)
        {
            int id_old_cluster = points[i].getCluster();
            float old_min_dist = points[i].getMinDist();
            int id_nearest_center = getIDNearestNonFullCenter(points[i]);
            if (id_old_cluster == -1) {
                points[i].setCluster(id_nearest_center);
                clusters[id_nearest_center].addPoint(points[i]);
                clusters[id_nearest_center].increaseSize();
                continue;
            }
            else {
                clusters[id_old_cluster].removePoint(points[i].getID());
                points[i].setCluster(id_nearest_center);
                clusters[id_nearest_center].addPoint(points[i]);
                clusters[id_nearest_center].increaseSize();
            }

        }
        float rss_new = 0.0;
        // recalculating the center of each cluster
        for(int i = 0; i < K; i++)
        {
            for(int j = 0; j < ndistinct_queries; j++)
            {
                int total_points_cluster = clusters[i].getTotalPoints();
                int sum = 0.0;

                if(total_points_cluster > 0)
                {
                    for(int p = 0; p < total_points_cluster; p++)
                        sum += clusters[i].getPoint(p).getValue(j);
                    float cur_ij_centroid = (float)sum / total_points_cluster;
                    clusters[i].setCentralValue(j, cur_ij_centroid);

                    for(int p = 0; p < total_points_cluster; p++)
                        rss_new += pow(clusters[i].getPoint(p).getValue(j) - cur_ij_centroid,2);
                }
            }
        }
        if ((rss - rss_new) / rss_new * 100 > th) {
            done = false;
            resetClusterSizeLimit();
        }
        rss = rss_new;
        if(done == true || iter >= max_iterations)
        {
//            cout << "Break in iteration " << iter << "\n";
            break;
        }

        iter++;
    }
    // shows elements of clusters
    /*
        for(int i = 4; i < 5; i++)
        {
            int total_points_cluster =  clusters[i].getTotalPoints();

            cout << "Cluster " << clusters[i].getID() + 1 << endl;
            for(int j = 0; j < total_points_cluster; j++)
            {
                cout << "Point " << clusters[i].getPoint(j).getID() + 1 << ": ";
                for(int p = 0; p < ndistinct_queries; p++)
                    cout << clusters[i].getPoint(j).getValue(p) << " ";

                cout << endl;
            }

            cout << "Cluster values: ";

            for(int j = 0; j < ndistinct_queries; j++)
                cout << clusters[i].getCentralValue(j) << " ";

            cout << "\n\n";
        }*/
    for (int i = 0; i < K; i++) {
        if(minmaxBoundaryEnabled){
            clusters[i].buildBoundary(data_dimension);
        }
        clusters[i].updateRangeFilter(queryMat);
    }
    return clusters;
}

bool Cluster::containsRecord(int id_point){
    for(auto iter = points->begin(); iter != points->end(); iter++){
        if(iter->getID() == id_point){
            return true;
        }
    }
    return false;
}

bool Cluster::queryIsIntersectedWithBoundary(int* query){
    if(boundary != NULL){
       for(int k = 0; k < data_dimension; k++){
           if(query[2*k] > boundary[k][1] || query[2*k+1] < boundary[k][0]){
               return false;
           }
       } 
    }
    return true;
}

bool Cluster::keyIsQualified(int* key){
    return rangeFilter.keyIsQualified(key);   
}

bool Cluster::queryIsQualified(int* query){
    return queryIsIntersectedWithBoundary(query) && rangeFilter.queryIsQualified(query) ;
    //return rangeFilter.queryIsQualified(query);
    //return queryIsIntersectedWithBoundary(query);
}

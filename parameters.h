//
// Created by Lina Qiu on 4/14/20.
//

#ifndef H_PART_PARAMETERS_H
#define H_PART_PARAMETERS_H

#include <vector>

using namespace std;

int data_dimension = 2;
int num_data = 20000;
int page_size = 250;
// range of data
int d_lower[] = {0, 0};
int d_higher[] = {255, 255};
// "uniform = 0", "normal = 1"
int data_distribution[] = {0, 0};
int num_queries = 400;
// query freq > 0 (smaller to have more distinct queries)
float freq_upper = 0.03;
int query_dimension = data_dimension;
// query selectivity target range
float s_lower = 0.1;
float s_higher = 0.3;

int ndistinct_queries; // number of distinct queries generated
vector<float> sel; // size = ndistinct_queries, query selectivity

int th = 0; // rss threshold for kmeans
int K = (num_data / page_size);

#endif //H_PART_PARAMETERS_H

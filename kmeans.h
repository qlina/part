//
// Created by Lina Qiu on 4/22/20.
//

#ifndef H_PART_KMEANS_H
#define H_PART_KMEANS_H

#include <iostream>
#include <vector>
#include <cmath>
#include <stdlib.h>
#include <time.h>
#include <algorithm>
#include "range_filter.h"

using namespace std;

class Point
{
private:
    int id_point, id_cluster;
    int* values;
//    vector<int> values;
    int* raw_values;
    int dimension;
    float min_dist;

public:
    Point(int id_point, int* values, int* raw_values, int dimension);
    int getID();
    void setCluster(int id_cluster);
    int getCluster();
    void setMinDist(float min_dist);
    float getMinDist();
    float getValue(int index);
    int getRawValue(int index);
    int getDimension();

//    void addValue(int value)
//    {
//        values.push_back(value);
//    }

//    string getName()
//    {
//        return name;
//    }
};

class Cluster
{
private:
    int id_cluster;
    int size;
    vector<float> central_values;
    int dimension;
    int data_dimension;
public:
    int** boundary;
    RangeFilter rangeFilter;
    vector<Point>* points;
    int* majority;
    float cos_similarity;

    Cluster(int id_cluster, Point point, int data_dimension);

    Cluster(int id_cluster, int data_dimension);
    void addPoint(Point point);
    bool removePoint(int id_point);
    float getCentralValue(int index);
    void setCentralValue(int index, float value);
    void resetSize();
    int getSize();
    void increaseSize();
    void decreaseSize();
    Point getPoint(int index);
    int getTotalPoints();
    int getID();
    void buildBoundary(int dimension);
    void computeMajority(int num_queries);
    void computeCosSimilarity();
    bool containsRecord(int id_point);
    void updateRangeFilter(int** queryMat);
    bool queryIsIntersectedWithBoundary(int* query);
    bool keyIsQualified(int* key);
    bool queryIsQualified(int* query);
};
// might have bias, data with smaller idx get assigned to its closet cluster
// TODO: shuffle data after every iteration
class KMeans
{
private:
    int K; // number of clusters
    int ndistinct_queries, total_points, max_iterations, cluster_size;
    bool minmaxBoundaryEnabled;
    float rss;
    vector<Cluster> clusters;
    // return ID of nearest center (uses euclidean distance)
    int getIDNearestNonFullCenter(Point& point);
    void resetClusterSizeLimit();

public:
    // total_values is point dimension
    KMeans(int K, int total_points, int total_values, int max_iterations, int cluster_size, bool minmaxBoundaryEnabled=true);
    // here the para dimension is used in buildBoundary()
    vector<Cluster> run(vector<Point>& points, int dimension, float th, int** queryMat);
};


#endif //H_PART_KMEANS_H

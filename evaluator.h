//
// Created by Lina Qiu on 4/21/20.
//

#ifndef H_PART_EVALUATOR_H
#define H_PART_EVALUATOR_H

#include <vector>
using namespace std;

class Evaluator {
public:
    int** queryMat;
    int dimension;
    vector<int>* freq_list;
    Evaluator(int** queryMat, int dimension, vector<int>* freq_list);
    int countQualifiedPartitions(int ndistinct_queries, vector<int>* bit_vectors);
    int countNonConsecutiveRuns(int ndistinct_queries, vector<int>* bit_vectors);
};


#endif //H_PART_EVALUATOR_H

//
// Created by Lina Qiu on 4/21/20.
//

#include <iostream>
#include "evaluator.h"

Evaluator::Evaluator(int **queryMat, int dimension, vector<int>* freq_list) {
    this->queryMat = queryMat;
    this->dimension = dimension;
    this->freq_list = freq_list;
}

int Evaluator::countQualifiedPartitions(int ndistinct_queries, vector<int>* bit_vectors) {
    int count = 0;
    for (int i = 0; i < ndistinct_queries; i++) {
        int q_count = 0;
        for (int j : bit_vectors[i]) {
            q_count += j;
        }
//        count += q_count;
        count += (q_count * (*freq_list)[i]);
    }
    return count;
}

int Evaluator::countNonConsecutiveRuns(int ndistinct_queries, vector<int>* bit_vectors) {
    int count = 0;
    int sum = 0;
    for (int i = 0; i < ndistinct_queries; i++) {
        int q_count = 0;
        for (int j : bit_vectors[i]) {
            if (j == 0 && sum > 0) {
                q_count += 1;
                sum = 0;
            }
            else
                sum += j;
        }
        count += (q_count * queryMat[i][dimension*2]);
        //count += q_count;
    }
    return count;
}

//
// Created by Lina Qiu on 4/15/20.
//

#ifndef H_PART_UTILITY_H
#define H_PART_UTILITY_H

#include <iostream>
using namespace std;

class Utility {
public:
    void printMat(int** mat, int d1, int d2);
    int** flipMatrix(int** cMat, int ndistinct_queries, int num_data);
};


#endif //H_PART_UTILITY_H

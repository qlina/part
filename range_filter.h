
#ifndef H_RANGE_FILTER_H
#define H_RANGE_FILTER_H

#include <vector>
#include <string>
using namespace std;

class RangeFilter {
     
     bool queryIsQualifiedForRange(int* query, int* range);
     bool queryIsInsideRange(int* query, int* range);
     bool keyMayQualified(int* key, vector<int*> ranges, bool existing_vs_every); // existing_vs_every true : there exists a range contains the key; false : every range has to contain the key
     bool queryMayQualified(int* query, vector<int*> ranges, bool existing_vs_every); // existing_vs_every true : there exists a range intersets with the query; false : every range has to intersect with the query
public:
     vector<int*> qualifiedRanges;
     vector<int*> unqualifiedRanges;

     int dimension;
     RangeFilter();
     RangeFilter(vector<int*> qualifiedRanges, vector<int*> unqualifiedRanges, int dimension);
     bool keyIsQualified(int* key);
     bool queryIsQualified(int* query);

     void print();

     static bool keyIsQualifiedForRange(int* key, int* range, int dimension);
};

#endif //H_RANGE_FILTER_H

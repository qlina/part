//
// Created by Lina Qiu on 4/1171/20.
//

#include <set>
#include <vector>
#include <iostream>
#include "partitioner.h"

Partitioner::Partitioner(vector<int>* query_ordering, vector<int>* freq_list, vector<int>* qMat, int num_data, int** queryMat, int** dataMat, short int** dMat, int** cMat, float** unweightedCostMat, int dimension, int num_empty_slots, bool minmaxBoundaryEnabled, bool nearestNeighborFillingEnabled) {
    this->query_ordering = query_ordering;
    this->freq_list = freq_list;
    this->qMat = qMat;
    this->num_data = num_data;
    this->queryMat = queryMat;
    this->dataMat = dataMat;
    this->dMat = dMat;
    this->cMat = cMat;
    this->unweightedCostMat = unweightedCostMat;
    this->dimension = dimension;
    this->num_empty_slots = num_empty_slots;
    this->minmaxBoundaryEnabled = minmaxBoundaryEnabled;
    this->nearestNeighborFillingEnabled = nearestNeighborFillingEnabled;
}

int* Partitioner::getCentralPoint(vector<int> data_idx){
   vector<int> agg (dimension, 0);
   for(int i = 0; i < data_idx.size(); i++){
      for(int j = 0; j < dimension; j++){
          agg[j] += dataMat[i][j];
      }
   }
   int* centralPoint = new int[dimension];
   for(int j = 0; j < dimension; j++){
      centralPoint[j] = agg[j]/data_idx.size();
   }
   return centralPoint;
}


void Partitioner::partitionData(int page_size, int strategy) {
    int level = 0;
    VirtualPartition* leaf_start;
    int t = 0;
    int q_idx = 0;
    int q_id = 0;
    set<int> unusedQueries = set<int> ();
    int ndistinct_queries = freq_list->size();
    for(int i = 0;i < ndistinct_queries; i++){
        unusedQueries.insert(i);
    }
    while(!unusedQueries.empty()) {
        if(strategy == 0){
            if(q_idx >= query_ordering->size()) break;
            q_id = query_ordering->at(q_idx);
            unusedQueries.erase(q_id);
            q_idx++;          
        } else if(strategy == 1){
            /* select query to be used for partitioning */ 
            // calculate the weighted cost and select the one with minimum cost
            int min = INT_MAX;
            for(int i : unusedQueries){
                float* costArray = unweightedCostMat[i];
                int tmp_cost = 0;
                for(int j : unusedQueries){
                    if(costArray[j] != 0){
                        tmp_cost += freq_list->at(j)*costArray[j];
                    }
                } 
                if(tmp_cost < min){
                   q_id = i;
                   min = tmp_cost;
                }
            }
            unusedQueries.erase(q_id);
            // update unweighted cost matrix
             
            short int* dependencyArray = dMat[q_id];
            float* initDeltaArray = unweightedCostMat[q_id];

            for(int i : unusedQueries){
                for(int j = 0; j < ndistinct_queries; j++){
                    if(unweightedCostMat[i][j] != 0){ // update non-zero cost
                        if(unweightedCostMat[i][j] > initDeltaArray[j]){ // the io cost is assumed as the minimum cost among query-based partitions
                            unweightedCostMat[i][j] = initDeltaArray[j];
                        }
                    }
                }
            }
        }

        real_ordering.push_back(q_id);


        int flag = 0;
        this->page_size = page_size;
        
        if (level == 0) {
            // the first split must exceed page size
            vector<int*> qualifiedRanges = vector<int*> (1, queryMat[q_id]); 
            vector<int*> unqualifiedRanges = vector<int*> (); 
            vector<int> complement = computeComplement(q_id, NULL);
            
            VirtualPartition *new_qualified_vp_p, *new_special_vp_p, *new_unqualified_vp_p;
            int remainings = qMat[q_id].size()%page_size;
            if(qMat[q_id].size() < page_size){
                new_qualified_vp_p = new VirtualPartition(qualifiedRanges, unqualifiedRanges, vector<int> (), dimension);
            }else{
                new_qualified_vp_p = new VirtualPartition(qualifiedRanges, unqualifiedRanges, vector<int> (qMat[q_id].begin()+remainings, qMat[q_id].end()), dimension);
            }
           
            if(remainings != 0){
                // Next: MIN HASH can be done here to further improve the performance
                vector<int> sortedComplement;
                vector<int> special_data_idx (qMat[q_id].begin(), qMat[q_id].begin() + remainings);
                int* tmpCentralPoint = getCentralPoint(special_data_idx);
                sortedComplement = sortByDistance(&complement, tmpCentralPoint);
                
                new_special_vp_p = new VirtualPartition(vector<int*> (), vector<int*> (), special_data_idx, dimension);
                new_unqualified_vp_p = new VirtualPartition(unqualifiedRanges, qualifiedRanges, sortedComplement, dimension);
                int count = 0;
                while (true) {
                    if(remainings + num_empty_slots >= page_size || count >= sortedComplement.size()) break;
                    int record_idx = sortedComplement[count];
                    new_special_vp_p->data_idx.push_back(record_idx);
                    new_unqualified_vp_p->data_idx.erase(new_unqualified_vp_p->data_idx.begin());
                    count++;
                    remainings++;
                }
                //updateRangeFilter(new_special_vp_p);
            }else{
                new_special_vp_p = new VirtualPartition(vector<int*> (), vector<int*> (), vector<int> (), dimension);
                new_unqualified_vp_p = new VirtualPartition(unqualifiedRanges, qualifiedRanges, complement, dimension);
            }
           
            root.qualified = new_qualified_vp_p;
            root.qualified->code = "0";
            root.special = new_special_vp_p;
            root.special->code = "1";
            root.unqualified = new_unqualified_vp_p;
            root.unqualified->code = "2";
            root.qualified->right = root.special;
            root.special->right = root.unqualified;
            // the leaf level
            leaf_start = root.qualified;
        }
        else {
            // split and have new leaf level
            VirtualPartition* pointer = leaf_start;
            VirtualPartition* curLeaf = NULL;
            leaf_start = NULL;
            while (pointer != NULL) {
                vector<int> intersect = computeIntersection(q_id, pointer);
                vector<int> complement = computeComplement(q_id, pointer);
                // page size limit
                if (pointer->data_idx.size() > page_size) {
                    flag = 1;
                    // small tune #leaves 48->23 #accesses 101066->611
                    // IGNORE: if (!(intersect.size() < page_size && (intersect.size() + complement.size()) <= 2*page_size)) {
                        // unqualified to not full qualified
                        vector<int*> qualifiedRanges = pointer->rangeFilter.qualifiedRanges;
                        vector<int*> unqualifiedRanges = pointer->rangeFilter.unqualifiedRanges;
                        qualifiedRanges.push_back(queryMat[q_id]); 
                        /** possible runtime improvement pointer->filled_data intersect with intersect **/
                        
                        VirtualPartition *new_qualified_vp_p, *new_special_vp_p, *new_unqualified_vp_p;
                        int remainings = intersect.size()%page_size;
                        if(intersect.size() < page_size){
                            new_qualified_vp_p = new VirtualPartition(qualifiedRanges, unqualifiedRanges,  vector<int>(), dimension);
                        }else{
                            new_qualified_vp_p = new VirtualPartition(qualifiedRanges, unqualifiedRanges, vector<int> (intersect.begin()+remainings, intersect.end()), dimension);
                        }
                        unqualifiedRanges.push_back(queryMat[q_id]);
                             
           
                        if(remainings != 0){
                            vector<int> sortedComplement;
                            
                            vector<int> special_data_idx (intersect.begin(), intersect.begin() + remainings);
                            int* tmpCentralPoint = getCentralPoint(special_data_idx);
                            sortedComplement = sortByDistance(&complement, tmpCentralPoint);
                            
                            new_special_vp_p = new VirtualPartition(pointer->rangeFilter.qualifiedRanges, pointer->rangeFilter.unqualifiedRanges, special_data_idx, dimension);
                            new_unqualified_vp_p = new VirtualPartition(pointer->rangeFilter.qualifiedRanges, unqualifiedRanges, sortedComplement, dimension);
                            
                            int count = 0;
                            while (true) {
                                if(remainings + num_empty_slots >= page_size || count >= sortedComplement.size()) break;
                                int record_idx = sortedComplement[count];
                                new_special_vp_p->data_idx.push_back(record_idx);
                                new_unqualified_vp_p->data_idx.erase(new_unqualified_vp_p->data_idx.begin());
                                count++;
                                remainings++;
                            }
                            //updateRangeFilter(new_special_vp_p);
	
                        }else{
                            new_special_vp_p = new VirtualPartition(pointer->rangeFilter.qualifiedRanges, pointer->rangeFilter.unqualifiedRanges, vector<int> (), dimension);
                            new_unqualified_vp_p = new VirtualPartition(pointer->rangeFilter.qualifiedRanges, unqualifiedRanges, complement, dimension);

                        }
 
                        
                       
                        pointer->qualified = new_qualified_vp_p;
                        pointer->qualified->code = pointer->code+string("0");
                        pointer->special = new_special_vp_p;
                        pointer->special->code = pointer->code+string("1");
                        pointer->unqualified = new_unqualified_vp_p;
                        pointer->unqualified->code = pointer->code+string("2");
                        pointer->qualified->right = pointer->special;
                        pointer->special->right = pointer->unqualified;
                        if (curLeaf != NULL) curLeaf->right = pointer->qualified;
                        if (leaf_start == NULL)  {
                            leaf_start = pointer->qualified;
                        }
                        curLeaf = pointer->unqualified;
                }
                pointer = pointer->right;
            }
        }
        level++;
        t += flag;
        if(leaf_start == NULL) break;
    }
    cout << "t is: " << t << endl;
    partitions = findLeaves(root);
}
/*
VirtualPartition* Partitioner::genSpecialVirtualPartition(vector<int> & qualified_data_idx, vector<int> & unqualified_data_idx){
    int ndistinct_queries = freq_list->size();
    int numHashFuncs = 40;
    uint32_t h = hash;
    const uint32_t delta = (h >> 17) | (h << 15);  // Rotate right 17 bits
    uint32_t b = (h % num_lines) * (cache_line_size * 8);

    for (uint32_t i = 0; i < numHashFuncs; ++i) {
      // Since CACHE_LINE_SIZE is defined as 2^n, this line will be optimized
           //  to a simple and operation by compiler.
               const uint32_t bitpos = b + (h % (cache_line_size * 8));
                   if (((data[bitpos / 8]) & (1 << (bitpos % 8))) == 0) {
                         return false;
                   }
      
            h += delta;
     } 
}
*/
vector<int> Partitioner::computeComplement(int q_id, VirtualPartition* parent) {
    vector<int> complement;
    if (parent == NULL) {
        for (int i = 0; i < num_data; i++) {
            if (!binary_search(qMat[q_id].begin(), qMat[q_id].end(), i))
                complement.push_back(i);
        }
    }
    else {
        for (int i : parent->data_idx) {
            if (!binary_search(qMat[q_id].begin(), qMat[q_id].end(), i))
                complement.push_back(i);
        }
    }
    return complement;
}

vector<int> Partitioner::computeIntersection(int q_id, VirtualPartition* parent) {
    vector<int> intersection;
    if (parent == NULL) {
        for (int i = 0; i < num_data; i++) {
            if (binary_search(qMat[q_id].begin(), qMat[q_id].end(), i))
                intersection.push_back(i);
        }
    }
    else {
        for (int i : parent->data_idx) {
            if (binary_search(qMat[q_id].begin(), qMat[q_id].end(), i))
                intersection.push_back(i);
        }
    }
    return intersection;
}

void Partitioner::updateRangeFilter(VirtualPartition* vp_p){
    if(vp_p->data_idx.size() == 0) return;
    int ndistinct_queries = freq_list->size();
    vector<bool> qualified_cVec_vp = vector<bool> (ndistinct_queries, true);
    vector<bool> unqualified_cVec_vp = vector<bool> (ndistinct_queries, false);
    for(int i = 0; i < vp_p->data_idx.size(); i++){
        for(int j = 0; j < ndistinct_queries; j++){
            qualified_cVec_vp[j] = qualified_cVec_vp[j] && cMat[j][i];
            unqualified_cVec_vp[j] = unqualified_cVec_vp[j] || cMat[j][i];
        }
    }
    for(int j = 0; j <ndistinct_queries; j++){
        int* query_idx = queryMat[j];
        if(qualified_cVec_vp[j]){
            vector<int*> tmpRanges = vp_p->rangeFilter.qualifiedRanges;
            if(find(tmpRanges.begin(), tmpRanges.end(), query_idx) == tmpRanges.end()){
                vp_p->rangeFilter.qualifiedRanges.push_back(query_idx);
            }
        }
        if(!unqualified_cVec_vp[j]){
            vector<int*> tmpRanges = vp_p->rangeFilter.unqualifiedRanges;
            if(find(tmpRanges.begin(), tmpRanges.end(), query_idx) == tmpRanges.end()){
                vp_p->rangeFilter.unqualifiedRanges.push_back(query_idx);
            }
        }
    }
    
}

vector<int> Partitioner::sortByDistance(vector<int> *complement, int* data) {
    vector<pair<float, int>> sortedComplementPair;
    for (int i = 0; i < complement->size(); i++) {
        int* c_data = dataMat[(*complement)[i]];
        float dis = 0;
        for (int j = 0; j < dimension; j++) {
            dis += pow((data[j] - c_data[j]),2);
        }
        dis = sqrt(dis);
        sortedComplementPair.push_back(pair<float, int>(dis, (*complement)[i]));
    }
    sort(sortedComplementPair.begin(), sortedComplementPair.end());
    vector<int> sortedComplement;
    for (int i = 0; i < complement->size(); i++) {
        sortedComplement.push_back(sortedComplementPair[i].second);
    }
    vector<pair<float, int>>().swap(sortedComplementPair); //free
    return sortedComplement;
}

vector<VirtualPartition> Partitioner::findLeaves(Root root) {
    vector<VirtualPartition> leaves;
    dfs(root.qualified, &leaves);
    
    if(root.special->data_idx.size() > 0){
        if(minmaxBoundaryEnabled){
            root.special->buildBoundary(dataMat);
        }
        leaves.push_back(*(root.special));
        updateRangeFilter(root.special);
    }
//    cout << "qualified leaves: " << leaves.size() << endl;
    dfs(root.unqualified, &leaves);
//    cout << "# partitions our solution create: " << leaves.size() << endl;
    for (int i = 0; i < leaves.size(); i++) {
//        cout << leaves[i].data_idx.size() << endl;
    }
//    cout << endl;
    return leaves;
}

int Partitioner::dfs(VirtualPartition* root, vector<VirtualPartition>* leaves) {
    if (root == NULL)
        return 0;
    int depth_left = dfs(root->qualified, leaves);
    if(root->special != NULL){
        if(root->special->data_idx.size() > 0){
            if(minmaxBoundaryEnabled){
                root->special->buildBoundary(dataMat);
            }
            (*leaves).push_back(*(root->special));
            updateRangeFilter(root->special);
        }
    }
    int depth_right = dfs(root->unqualified, leaves);
    int cur_depth = max(depth_left, depth_right);
    if (cur_depth == 0 && root->data_idx.size()>0) {
        if(minmaxBoundaryEnabled){
            root->buildBoundary(dataMat);
        }
        (*leaves).push_back(*root);
        updateRangeFilter(root);
    }
    return (cur_depth+1);
}
